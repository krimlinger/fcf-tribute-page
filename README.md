# fcf-tribute-page

A tribute to RMS, based on fcf's projects.

The code is distributed under AGPLv3 or later.

RMS's image is under [GNU Free Documentation License, Version 1.2 or later](https://commons.wikimedia.org/wiki/Commons:GNU_Free_Documentation_License,_version_1.2).
